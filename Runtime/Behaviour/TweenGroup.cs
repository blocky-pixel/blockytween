﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlockyLib.Tween
{
    public class TweenGroup : MonoBehaviour
    {
        public GameObject[] remoteTargets;
        public bool disableOnHide = true;
        public bool startShown = false;
        public bool resetOnDisable = true;

        private List<TweenBehaviour> tweens = new List<TweenBehaviour>();

        private int activeTweens;
        private bool _isShown;
        public bool isShown { get { return _isShown; } }
        public bool isPlaying { get { return activeTweens > 0; } }

        private Queue<System.Action> callback = new Queue<System.Action>();

        private bool isAwake = false;

        void Awake()
        {
            if (isAwake) return;
            isAwake = true;
            _isShown = startShown;

            LoadTweens();

            if (!_isShown && disableOnHide)
                EnableGameObjects(false);
        }

        void OnDestroy()
        {
            UnloadTweens();
        }

        void OnDisable()
        {
            if (!isAwake) return;
            CheckResetOnDisable();
        }

        void CheckResetOnDisable()
        {
            if (!resetOnDisable) return;

            _isShown = startShown;
            foreach (var t in tweens)
            {
                if (!t) continue;
                t.Reset(!_isShown);
                
                if (disableOnHide && t.gameObject.activeSelf != _isShown && t.gameObject != gameObject) 
                    t.gameObject.SetActive(_isShown);
            }

            var callbacks = callback.ToArray(); //we might see callbacks add to the queue, make a copy to that messing with anything
            callback.Clear();
            foreach (var c in callbacks) c();
        }

        void UnloadTweens()
        {
            foreach (var tween in tweens)
            {
                if (tween == null) continue; //destroyed
                tween.events.onPlay.RemoveListener(OnTweenPlay);
                tween.events.onStop.RemoveListener(OnTweenStop);
            }

            tweens.Clear();
        }

        void LoadTweens()
        {
            tweens.AddRange(GetComponents<TweenBehaviour>());
            foreach (var remote in remoteTargets)
            {
                if (!remote) continue;
                tweens.AddRange(remote.GetComponents<TweenBehaviour>());
            } 

            foreach (var tween in tweens)
            {
                if (!tween)
                {
                    Debug.LogErrorFormat(gameObject, "Null tween in tween group {0}", name);
                    continue;
                }

                if (tween.mode != TweenBehaviour.Mode.Once)
                {
                    Debug.LogWarning("Tweens used in a tween group must be set to the mode 'once'", tween.gameObject);
                    tween.mode = TweenBehaviour.Mode.Once;
                }

                tween.playOnEnable = false;
                tween.resetOnDisable = false;
                tween.Reset(!_isShown);

                tween.events.onPlay.AddListener(OnTweenPlay);
                tween.events.onStop.AddListener(OnTweenStop);
            }
        }

        public void Show(System.Action callback)
        {
            Show(true, callback);
        }

        public void Show()
        {
            Show(true, null);
        }

        public void ShowImmediate()
        {
            ShowImmediate(true);
        }
        
        public void Hide()
        {
            Show(false, null);
        }

        public void Hide(System.Action callback)
        {
            Show(false, callback);
        }

        public void HideImmediate()
        {
            ShowImmediate(false);
        }

        public void ShowImmediate(bool show)
        {
            if (!isAwake) Awake(); //load the group
            if (show) EnableGameObjects(true); //make sure everything is turned on to play the tween

            if (activeTweens > 0)
            {
                //Stop any existing tweens, this is lower our active tween count if we're playing
                //This with also call any outstanding callbacks for when we complete
                foreach (var tween in tweens)
                    if (tween.isPlaying) tween.Stop(); 
            }

            if (activeTweens > 0)
            {
                //Well, something has overriden us as part of a callback - just abort
                return;
            }

            _isShown = show;
            if (show) EnableGameObjects(true); //make sure everything is turned on to play the tween
            foreach (var tween in tweens) tween.SetProgress(show ? 1 : 0);

            CompleteTween();
        }

        public void Show(bool show, System.Action onComplete = null)
        {
            if (!isAwake) Awake(); //load the group
            if (show) EnableGameObjects(true); //make sure everything is turned on to play the tween

            if (!gameObject.activeInHierarchy)
            { //we can't play when disabled, so when not in an enabled state, use the immediate version
                ShowImmediate(show);
                return;
            }

            if (activeTweens > 0)
            { //transition in progress
                if (_isShown == show) //already in progress, just queue the oncomplete if we have one
                {
                    if (onComplete != null) callback.Enqueue(onComplete);
                    return;
                }
                else
                { //we're going the other way, clear the exiting queue
                    while (callback.Count > 0) callback.Dequeue()();
                }
            }

            _isShown = show;
            
            if (tweens.Count == 0)
            { //nothing to tween
                if (onComplete != null) onComplete();
                return;
            }

            if (onComplete != null) callback.Enqueue(onComplete);

            foreach (var tween in tweens)
            {
                var progress = tween.GetProgress();

                if (tween.isPlaying)
                    tween.Reverse();
                else if (progress != (_isShown ? 1 : 0))
                    tween.ResetAndPlay(_isShown);
            }

            if (activeTweens == 0) CompleteTween(); //nothing started playing
        }

        private void OnTweenStop()
        {
            activeTweens--;

            if (activeTweens == 0)
                CompleteTween();
        }

        private void OnTweenPlay()
        {
            activeTweens++;
        }

        private void CompleteTween()
        {
            var callbacks = callback.ToArray(); //we might see callbacks add to the queue, make a copy to that messing with anything
            callback.Clear();
            foreach (var c in callbacks) c();

            if (activeTweens > 0) return; //we've started again

            if (disableOnHide && !_isShown)
                EnableGameObjects(false);
        }

        private void EnableGameObjects(bool isEnabled)
        {
             foreach (var tween in tweens) 
                if (tween.gameObject.activeSelf != isEnabled) tween.gameObject.SetActive(isEnabled);

            gameObject.SetActive(isEnabled);
        }
    }
}
