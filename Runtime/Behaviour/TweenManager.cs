﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlockyLib.Tween
{
    public class TweenManager : MonoBehaviour
    {
        public interface ITimeProvider
        {
            float deltaTime { get; }
            float unscaledDeltaTime { get; }
            float time { get; }
            float unscaledTime { get; }
        }

        public class DefaultTimeProvider : ITimeProvider
        {
            public float deltaTime => UnityEngine.Time.deltaTime;

            public float unscaledDeltaTime => UnityEngine.Time.unscaledDeltaTime;

            public float time => UnityEngine.Time.time;

            public float unscaledTime => UnityEngine.Time.unscaledTime;
        }

        private static ITimeProvider DEFAULT_TIME_PROVIDER = new DefaultTimeProvider();
        public static ITimeProvider customTimeProvider;
        public static ITimeProvider TimeProvider {  get { return customTimeProvider == null ? DEFAULT_TIME_PROVIDER : customTimeProvider; } }

        private static TweenManager instance;
        private static bool hasCreatedInstance;
        public static bool isDestroyed { get { return hasCreatedInstance && instance == null; } }

        private bool isExecuting;
        private TweenNode tweenUpdate;
        private TweenNode tweenLateUpdate;
        private TweenNode tweenFixedUpdate;
        private TweenNode nodePool;

        public class TweenNode 
        {
            public TweenNode previous;
            public TweenNode next;
            public TweenBehaviour.UpdateType updateType; //save this off incase it's changed in the behaviour
            public TweenBehaviour tween;
        }
        
        private static void CreateInstance()
        {           
            if (hasCreatedInstance) return;

            hasCreatedInstance = true;
            instance = new GameObject().AddComponent<TweenManager>();
            instance.name = "TweenManager";
            GameObject.DontDestroyOnLoad(instance.gameObject);
        }

        void Awake()
        {
            if (!hasCreatedInstance || (instance != null && instance != this))
            {
                Debug.LogWarning("Multiple instances of tween manager detected. These are created automatically. Do not add to scene manually");
                GameObject.Destroy(this);
                return;
            }
        }

        void OnDestroy()
        {
            if (instance == this)
                instance = null;
        }

        void Update()
        {
            if (tweenUpdate == null) return;
            UpdateList(tweenUpdate);
        }

        void FixedUpdate()
        {
            if (tweenFixedUpdate == null) return;
            UpdateList(tweenFixedUpdate);
        }

        void LateUpdate()
        {
            if (tweenLateUpdate == null) return;
            UpdateList(tweenLateUpdate);
        }

        private Queue<TweenNode> stopQueue = new Queue<TweenNode>();
        private Queue<TweenNode> migrateQueue = new Queue<TweenNode>();
        private void UpdateList(TweenNode first)
        {
            Debug.Assert(!instance.isExecuting, "Cannoy execute multiple tween lists at once");
            instance.isExecuting = true;
            
            var current = first;
            var dt = TimeProvider.deltaTime;
            var unscaledDt = TimeProvider.unscaledDeltaTime;

            stopQueue.Clear();
            while (current != null)
            {
                try
                {
                    current.tween.UpdateTween(current.tween.ignoreTimeScale ? unscaledDt : dt);
                }
                catch (System.Exception ex)
                {
                    Debug.LogError("Error updating tween: Stopping immediately.");
                    Debug.LogError(ex);
                    stopQueue.Enqueue(current);
                }

                current = current.next;
            }

            instance.isExecuting = false;

            while (stopQueue.Count > 0)
                Stop(stopQueue.Dequeue());

            while (migrateQueue.Count > 0)
                MigrateUpdateType(migrateQueue.Dequeue());
        }

        private TweenNode GetNode()
        {
            if (nodePool == null)
                return new TweenNode();
            
            var newNode = nodePool;
            nodePool = nodePool.next;
            newNode.next = null;

            return newNode;
        }

        private void ReturnNodeToPool(TweenNode node)
        {
            node.next = nodePool;
            node.previous = null;
            node.tween = null;
            nodePool = node;
        }

        private static void RemoveNode(TweenNode node)
        {
             if (node.previous == null) //head node, set the next node
                instance.SetFirst(node.updateType, node.next);

            //Remove the node
            if (node.next != null) node.next.previous = node.previous;
            if (node.previous != null) node.previous.next = node.next;
        }
        
        private static void AddNode(TweenNode node)
        {
            var first = instance.GetFirst(node.updateType);
            if (first != null) 
            {
                node.next = first;
                first.previous = node;
            }

            instance.SetFirst(node.updateType, node);
        }

        public static TweenNode Play(TweenBehaviour tweenBehaviour)
        {
            if (isDestroyed || !Application.isPlaying) return null;

            if (!hasCreatedInstance) CreateInstance();

            var node = instance.GetNode();
            node.tween = tweenBehaviour;
            node.updateType = tweenBehaviour.GetUpdateType();
            AddNode(node);

            return node;
        }

        public static void Stop(TweenNode node)
        {
            if (isDestroyed || !Application.isPlaying) return;

            if (instance.isExecuting)
            {
                instance.stopQueue.Enqueue(node);
                return;
            }

            RemoveNode(node);
            instance.ReturnNodeToPool(node);
        }

        public static void MigrateUpdateType(TweenNode node)
        {
            if (!node.tween) return; //node has been cleared

            if (instance.isExecuting)
            {
                instance.migrateQueue.Enqueue(node);
                return;
            }

            RemoveNode(node);
            AddNode(node); //readd it to the new update type
        }


        private TweenNode GetFirst(TweenBehaviour.UpdateType updateType)
        {
            if (updateType == TweenBehaviour.UpdateType.Update) return tweenUpdate;
            else if (updateType == TweenBehaviour.UpdateType.LateUpdate) return tweenLateUpdate;
            else if (updateType == TweenBehaviour.UpdateType.FixedUpdate) return tweenFixedUpdate;
            else return null;
        }

        private void SetFirst(TweenBehaviour.UpdateType updateType, TweenNode node)
        {
            if (updateType == TweenBehaviour.UpdateType.Update) tweenUpdate = node;
            else if (updateType == TweenBehaviour.UpdateType.LateUpdate) tweenLateUpdate = node;
            else if (updateType == TweenBehaviour.UpdateType.FixedUpdate) tweenFixedUpdate = node;
        }

    }
}
