﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlockyLib.Tween
{
    public abstract class TweenBehaviour : MonoBehaviour
    {
         public enum UpdateType
        {
            Update = 1,
            LateUpdate = 2,
            FixedUpdate = 3,
			Manual = 4
        }

        public enum Mode
        {
            Once = 1,
            Loop = 2,
            PingPong = 3
        }

        [System.Serializable]
        protected struct TweenState
        {
            public float time;
            public float delay;
            public int loopCycle;
            public bool isPlaying;
            public bool sentFirstPlayEvent; //not used in SyncLoop
            public bool isForward;
            public bool playOnNextEnable;
            public bool stopOnNextCycle;
            public TweenManager.TweenNode node;
        }

        [System.Serializable]
        public struct TweenEvents
        {
            public UnityEngine.Events.UnityEvent onPlay;
            public UnityEngine.Events.UnityEvent onStop;

            public UnityEngine.Events.UnityEvent onBegin;
            public UnityEngine.Events.UnityEvent onEnd;
        }

        public bool isPlaying { get { return isInit && state.isPlaying; } }
        public bool hasPendingStop { get { return isInit && state.stopOnNextCycle; } }

        //Configuration
        public Mode mode = Mode.Once;

        [SerializeField] private UpdateType updateType = UpdateType.Update;

        public float delay = 0f;
        public float duration = 1f;

        public bool playOnEnable = true;
        public bool resetOnDisable = true;
        public bool ignoreTimeScale = false;
        public bool syncLoop = false;
        public float syncLoopOffset = 0f;

        public AnimationCurve curve = AnimationCurve.Linear(0,0,1,1);

        //Events
        public TweenEvents events;
        
        //Tween State
        private bool isInit;
        protected TweenState state;

        private void Init()
        {
            if (isInit) return;
            isInit = true;
            OnInit();
            ResetTween();
        }

        protected abstract void OnInit();
        protected virtual void OnStop() { }
        protected virtual void OnPlay() { }

        void Awake()
        {
            if (!isInit) Init();
        }

        void OnDestroy()
        {
            if (state.isPlaying) Stop();
        }

        void OnEnable()
        {
            if (state.playOnNextEnable)
            {
                state.playOnNextEnable = false;
                Play();
            } 
        }

        void OnDisable()
        {
            if (playOnEnable)
                state.playOnNextEnable = true;

            if (state.isPlaying) Stop();

            if (isInit && resetOnDisable)
                Reset();
        }

        protected float Evaluate(float offset)
        {
            if (!state.isPlaying || duration == 0 || !isInit) 
            { //can't move a tween that isn't playing, or of zero length (or not init)
                offset = 0;
            }

            if (offset == 0) return curve.Evaluate(GetProgress());
            if (state.delay >= offset) return curve.Evaluate(state.time / duration); //haven't started yet
            
            if (state.delay > 0) //factor in the delay into the offset
                offset -= state.delay;

            var isForward = state.isForward;
            var time = state.time;
            time += (isForward ? offset : -offset);
            time /= duration;
            
            if (mode == Mode.Once)
            {
                time = Mathf.Clamp01(time);
            }
            else if (mode == Mode.Loop)
            {
                time %= 1;
                if (time < 0) time += 1f;
            }
            else
            {
                var isAltPhase = (time < 0 ? (int)time % 2 == 0 : (int)time % 2 == 1);
                time %= 1f;
                if (time < 0) time += 1f;
                if (isAltPhase) time = 1f - time;
            }

            return curve.Evaluate(time);
        }

        private void ProcessCycleCompleteEvents()
        {
            if (mode == Mode.Loop)
            {
                if (state.isForward) 
                {
                    //Snap back to the beginning
                    ProcessOnEnd();
                    ProcessOnBegin();
                }
                else 
                {
                    //Snap back to the end
                    ProcessOnBegin();
                    ProcessOnEnd();
                }
            }
            else
            {
                if (state.isForward) ProcessOnEnd();
                else ProcessOnBegin();
            }
        }

        public void UpdateTween(float dt)
        {
            if (!state.isPlaying || duration == 0 || !isInit) return;

            if (state.delay > 0)
            {
                state.delay -= dt;
                if (state.delay < 0)
                {
                    dt = -state.delay; //set the remainder to the current delta time
                    state.delay = 0;
                }
                else
                { //Keep waiting
                    SetProgress(state.time / duration);
                    return;
                }
            }

            if (mode != Mode.Once && syncLoop)
            {
                var prevCycle = state.loopCycle;

                var playbackTime = ignoreTimeScale ? TweenManager.TimeProvider.unscaledTime : TweenManager.TimeProvider.time;
                playbackTime += syncLoopOffset;

                state.time = playbackTime % duration;
                state.loopCycle = Mathf.FloorToInt(playbackTime / duration);
                state.isForward = (mode == Mode.Loop || (mode == Mode.PingPong && state.loopCycle % 2 == 0));
                if (!state.isForward) state.time = duration - state.time; //flip it

                if (prevCycle != state.loopCycle)
                {
                    ProcessCycleCompleteEvents();
                }
            }
            else
            {
                //If this is the first time we've processed this tween, we want to see
                //if we have any events to fire on the first play 
                if (!state.sentFirstPlayEvent)
                {
                    state.sentFirstPlayEvent = true;
                    if (state.time == 0) ProcessOnBegin();
                    else if (state.time == duration) ProcessOnEnd();
                }

                state.time += (state.isForward ? dt : -dt);

                var didFinishTween = false;
                if ((state.isForward && state.time >= duration) || (!state.isForward && state.time <= 0))
                {
                    ProcessCycleCompleteEvents();                    
                    didFinishTween = true;
                }

                if (didFinishTween)
                {
                    if (mode == Mode.Once)
                    {
                        state.time = (state.isForward ? duration : 0);
                        Stop();
                    }
                    else if (mode == Mode.Loop)
                    {
                        state.time = state.time % duration;
                        state.loopCycle++;

                        if (!state.isForward) state.time += duration;
                    }
                    else if (mode == Mode.PingPong)
                    {
                        state.isForward = !state.isForward; //flip the direction
                        state.loopCycle++;

                        if (state.isForward)
                            state.time = -state.time; //we must have gone into the negative, just flip it
                        else
                            state.time = duration - (state.time % duration); //Going backwards now, subtract the overflow
                    }
                }
            }

            var t = state.time / duration;
            SetProgress(t);
        }

        [ContextMenu("Play")]
        public void Play()
        {
            if (!isInit) Init();

            if (state.isPlaying) return;
            if (TweenManager.isDestroyed) return; //cannot play once the singleton is destroyed

            if (!isActiveAndEnabled)
            {
                state.playOnNextEnable = true;
                return;
            }

            state.isPlaying = true;
            state.delay = delay;
            state.node = TweenManager.Play(this);
            
            ProcessOnPlay();
        }

        [ContextMenu("Reverse")]
        public void Reverse()
        {
            if (!isInit) Init();
            state.isForward = !state.isForward;
        }

        public void ResetAndPlay(bool fromBeginning = true)
        {
            if (!isInit) Init();

            Reset(fromBeginning);
            state.isForward = fromBeginning;
            Play();
        }

        [ContextMenu("Stop")]
        public void Stop()
        {
            if (!Application.isPlaying) return;
            if (!isInit) Init();

            if (!gameObject.activeSelf)
                state.playOnNextEnable = false; //if we're not enabled but scheduled to play on next enable, disable that now

            state.stopOnNextCycle = false; //make sure to clear this flag

            if (!state.isPlaying) return;
            state.isPlaying = false;

            TweenManager.Stop(state.node);
            state.node = null;

            ProcessOnStop();
        }

        [ContextMenu("Stop on next cycle")]
        public void StopOnNextCycle()
        {
            state.stopOnNextCycle = true;
        }

        [ContextMenu("Clear pending stop")]
        public void ClearPendingStop()
        {
            state.stopOnNextCycle = false;
        }

        public void Reset(bool beginning = true) //Unityism alert - calling this "Reset()" will it invoked by the editor
        {
            if (!isInit) Init();
            ResetTween(beginning);
        }

        private void ResetTween(bool beginning = true)
        {
            if (state.isPlaying) Stop();

            state.isForward = true;
            state.stopOnNextCycle = false;
            state.loopCycle = 0;
            state.sentFirstPlayEvent = false;
            state.time = beginning ? 0 : duration;
            state.playOnNextEnable = playOnEnable;

            if (duration > 0) SetProgress(state.time / duration);
        }

        public void SetProgress(float progress)
        {
            if (!isInit) Init();
            state.time = progress * duration;
            SetTweenProgress(curve.Evaluate(progress));
        }

        public float GetProgress()
        {
            if (!isInit) Init();

            return state.time / duration;
        }

        public UpdateType GetUpdateType()
        {
            return updateType;
        }

        public void SetUpdateType(UpdateType type)
        {
            if (updateType == type) return;
            updateType = type; 

            if (state.node != null)
            {
                state.node.updateType = updateType;
                TweenManager.MigrateUpdateType(state.node);
            }
        }

        private void ProcessOnEnd()
        {
             events.onEnd.Invoke();

            if (state.isForward && state.stopOnNextCycle && (mode == Mode.Loop || mode == Mode.Once))
                Stop();
        }

        private void ProcessOnBegin()
        {
             events.onBegin.Invoke();
             
            if (!state.isForward && state.stopOnNextCycle && (mode == Mode.Loop || mode == Mode.Once))
                Stop();
            else if (state.stopOnNextCycle && mode == Mode.PingPong)
                Stop();
        }

        private void ProcessOnStop()
        {
            events.onStop.Invoke();
            OnStop();
        }

        private void ProcessOnPlay()
        {
            events.onPlay.Invoke();
            OnPlay();
        }

        protected abstract void SetTweenProgress(float progress);
    }

    public abstract class TweenBehaviour<TModule, TTarget> : TweenBehaviour
        where TModule : BaseTweenModule<TTarget>
        where TTarget : Component
    {
        [SerializeField] protected TModule tween;

        public TTarget target { get; private set; }
        public TModule GetTween() { return tween; }

        override protected void OnInit()
        {
            target = GetComponent<TTarget>();
            tween.Init(target);
        }
       
        override protected void SetTweenProgress(float progress)
        {
            tween.Apply(progress);
        }

        protected void SaveAsFrom()
        {
            if (!Application.isPlaying || !tween.isInit) OnInit();
            tween.SaveAsFrom();
            
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(gameObject);
#endif
        }

        protected void SaveAsTo()
        {
            if (!Application.isPlaying || !tween.isInit) OnInit();
            tween.SaveAsTo();

#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(gameObject);
#endif
        }
    }
}