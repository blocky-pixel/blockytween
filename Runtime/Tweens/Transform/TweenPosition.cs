﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlockyLib.Tween
{
    public class TweenPosition : TweenTransformBehaviour<TweenPositionModule>
    {
        [ContextMenu("Read From")] private void ReadFrom() { SaveAsFrom(); }
        [ContextMenu("Read To")] private void ReadTo() { SaveAsTo(); }
    }
    
    [System.Serializable]
    public class TweenPositionModule : TweenTransformModule
    {
        public Vector3 from;
        public Vector3 to;
        public bool isToOffset;

        private Rigidbody rigidbody;
        private RectTransform rectTransform;

        private TransformMode mode;

        private enum TransformMode
        {
            RigidBody,
            RectTransform,
            Transform
        }

        override protected void OnInit()
        {
            base.OnInit();

            rigidbody = target.GetComponent<Rigidbody>();
            rectTransform = target as RectTransform;

            if (rigidbody != null) mode = TransformMode.RigidBody;
            else if (rectTransform != null) mode = TransformMode.RectTransform;
            else mode = TransformMode.Transform;
        }
        
        override protected void OnClear()
        {
            base.OnClear();
            
            rigidbody = null;
            rectTransform = null;
        }

        public override void Apply(float value)
        {
            var position = Vector3.LerpUnclamped(from, (isToOffset ? from + to : to), value);

            #if UNITY_EDITOR
            if (!Application.isPlaying)
            { //always use the local position for previewing
                if (xForm != null) 
                {
                    if (xForm is RectTransform) (xForm as RectTransform).anchoredPosition = position;
                    else xForm.localPosition = position;
                }
                return;
            }
            #endif

            if (mode == TransformMode.Transform)
                xForm.localPosition = position;
            else if (mode == TransformMode.RigidBody)
                rigidbody.MovePosition(xForm.parent == null ? position : xForm.parent.TransformPoint(position));
            else
                rectTransform.anchoredPosition = (Vector2)position;
        }

        override public void SaveAsFrom()
        {
            if (target is RectTransform)
                from = (target as RectTransform).anchoredPosition;
            else
                from = target.localPosition;
        }

        override public void SaveAsTo()
        {
            if (target is RectTransform)
                to = (target as RectTransform).anchoredPosition;
            else
                to = target.localPosition;

            if (isToOffset)
                to -= from;
        }

        public override Matrix4x4 EvaluateLocalMatrix(float value)
        {
            var position = Vector3.LerpUnclamped(from, (isToOffset ? from + to : to), value);
            return Matrix4x4.TRS(position, xForm.localRotation, xForm.localScale);
        }
    }
}
