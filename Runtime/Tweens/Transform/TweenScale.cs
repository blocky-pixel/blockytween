﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlockyLib.Tween
{
    public class TweenScale : TweenBehaviour<TweenScaleModule, Transform>
    {
        [ContextMenu("Read From")] private void ReadFrom() { SaveAsFrom(); }
        [ContextMenu("Read To")] private void ReadTo() { SaveAsTo(); }
    }

    [System.Serializable]
    public class TweenScaleModule : BaseTweenModule<Transform>
    {
        private Transform xForm;

        public Vector3 from;
        public Vector3 to;

        override protected void OnInit()
        {
            xForm = target;
        }

        override protected void OnClear()
        {
            xForm = null;
        }

        public override void Apply(float value)
        {
            xForm.localScale = Vector3.LerpUnclamped(from, to, value);
        }

        override public void SaveAsFrom()
        {
            from = target.localScale;
        }

        override public void SaveAsTo()
        {
            to = target.localScale;
        }
    }
}
