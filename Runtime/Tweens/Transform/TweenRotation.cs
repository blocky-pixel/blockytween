﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlockyLib.Tween
{
    public class TweenRotation : TweenTransformBehaviour<TweenRotationModule>
    {
        [ContextMenu("Read From")] private void ReadFrom() { SaveAsFrom(); }
        [ContextMenu("Read To")] private void ReadTo() { SaveAsTo(); }
    }
    
    [System.Serializable]
    public class TweenRotationModule : TweenTransformModule
    {
        public Vector3 from;
        public Vector3 to;

        private Rigidbody rigidbody;

        private TransformMode mode;

        private enum TransformMode
        {
            RigidBody,
            Transform
        }

        override protected void OnInit()
        {
            base.OnInit();
            rigidbody = target.GetComponent<Rigidbody>();

            if (rigidbody != null) mode = TransformMode.RigidBody;
            else mode = TransformMode.Transform;
        }

        override protected void OnClear()
        {
            base.OnClear();
            rigidbody = null;
        }

        public override Matrix4x4 EvaluateLocalMatrix(float value)
        {
            var localRotation = Quaternion.Euler(Vector3.LerpUnclamped(from, to, value));
            return Matrix4x4.TRS(xForm.localPosition, localRotation, xForm.localScale);
        }

        public override void Apply(float value)
        {
            var localRotation = Quaternion.Euler(Vector3.LerpUnclamped(from, to, value));

            #if UNITY_EDITOR
            if (!Application.isPlaying)
            { //always use the local position for previewing
                if (xForm != null) xForm.localRotation = localRotation;
                return;
            }
            #endif

            if (mode == TransformMode.RigidBody)
                rigidbody.MoveRotation(xForm.parent == null ? localRotation : xForm.parent.rotation * localRotation);
            else
                xForm.localRotation = localRotation;
        }

        override public void SaveAsFrom()
        {
            from = target.localEulerAngles;
        }

        override public void SaveAsTo()
        {
            to = target.localEulerAngles;
        }
    }
}
