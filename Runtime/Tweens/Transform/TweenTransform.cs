﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlockyLib.Tween
{
    public interface ITweenTransformBehaviour
    {
        Matrix4x4 EvaluateLocalMatrix(float offset);
    }

    public abstract class TweenTransformBehaviour<TModule> : TweenBehaviour<TModule, Transform>, ITweenTransformBehaviour
        where TModule : TweenTransformModule
    {
        public Matrix4x4 EvaluateLocalMatrix(float offset)
        {
            return tween.EvaluateLocalMatrix(Evaluate(offset));
        }
    }
    
    [System.Serializable]
    public abstract class TweenTransformModule : BaseTweenModule<Transform>
    {
        protected Transform xForm;

        override protected void OnInit()
        {
            xForm = target;
        }

        override protected void OnClear()
        {
            xForm = null;
        }

        public abstract Matrix4x4 EvaluateLocalMatrix(float offset);
    }
}
