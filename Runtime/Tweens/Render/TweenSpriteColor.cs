﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlockyLib.Tween.Render
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class TweenSpriteColor : TweenBehaviour<TweenSpriteColorModule, SpriteRenderer>
    {
    }

    [System.Serializable]
    public class TweenSpriteColorModule : BaseTweenModule<SpriteRenderer>
    {
        public Color from = Color.white;
        public Color to = Color.white;

        public override void Apply(float value)
        {
            var color = Color.Lerp(from, to, value);
            target.color = color;
        }
    }
}