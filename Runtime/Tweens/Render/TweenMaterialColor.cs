﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlockyLib.Tween.Render
{
    [RequireComponent(typeof(Renderer))]
    public class TweenMaterialColor : TweenBehaviour<TweenMaterialColorModule, Renderer>
    {
    }

    [System.Serializable]
    public class TweenMaterialColorModule : BaseTweenModule<Renderer>
    {
        public string propertyName = "_BaseColor";
        public bool disableRendererWhenHidden = true;
        public Color from = Color.white;
        public Color to = Color.white;
	    private MaterialPropertyBlock materialBlock;

        protected override void OnInit()
        {
            materialBlock = new MaterialPropertyBlock();
        }

        public override void Apply(float value)
        {
            if (!Application.isPlaying) return; //not supported when not running

            var color = Color.Lerp(from, to, value);
            if (disableRendererWhenHidden) target.enabled = color.a > 0;
            if (!target.enabled) return;

            target.GetPropertyBlock(materialBlock);
			materialBlock.SetColor(propertyName, color);
            target.SetPropertyBlock(materialBlock);
        }
    }
}