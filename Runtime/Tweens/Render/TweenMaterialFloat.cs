using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlockyLib.Tween.Render
{
    [RequireComponent(typeof(Renderer))]
    public class TweenMaterialFloat : TweenBehaviour<TweenMaterialFloatModule, Renderer>
    {
    }

    [System.Serializable]
    public class TweenMaterialFloatModule : BaseTweenModule<Renderer>
    {
        public string propertyName = "Property";       
        public float from = 0f;
        public float to = 1f;

	    private MaterialPropertyBlock materialBlock;

        protected override void OnInit()
        {
            materialBlock = new MaterialPropertyBlock();
        }

        public override void Apply(float value)
        {
            if (!Application.isPlaying) return; //not supported when not running

            target.GetPropertyBlock(materialBlock);
			materialBlock.SetFloat(propertyName, Mathf.Lerp(from, to, value));
            target.SetPropertyBlock(materialBlock);
        }
    }
}