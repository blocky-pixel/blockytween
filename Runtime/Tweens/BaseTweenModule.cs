﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlockyLib.Tween
{
    public abstract class BaseTweenModule
    {
        public virtual void SaveAsFrom() {}
        public virtual void SaveAsTo() {}
        public virtual void Reset() {}
    }

    public abstract class BaseTweenModule<T> : BaseTweenModule
        where T : Object
    {
        public T target { get; private set; }
        public bool isInit { get; private set; }
        
        protected virtual void OnInit() {}
        protected virtual void OnClear() {}
        public abstract void Apply(float value);
        
        public void Init(T target)
        {
            this.target = target;
            isInit = true;
            OnInit();
        }

        public void Clear()
        {
            isInit = false;
            target = null;
            OnClear();
        }
    }
}
