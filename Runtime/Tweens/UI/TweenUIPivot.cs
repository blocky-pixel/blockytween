﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlockyLib.Tween
{
    [RequireComponent(typeof(RectTransform))]
    public class TweenUIPivot : TweenBehaviour<TweenUIPivotModule, RectTransform>
    {
        [ContextMenu("Read From")] private void ReadFrom() { SaveAsFrom(); }
        [ContextMenu("Read To")] private void ReadTo() { SaveAsTo(); }
    }

    [System.Serializable]
    public class TweenUIPivotModule : BaseTweenModule<RectTransform>
    {
        public Vector2 from;
        public Vector2 to;

        public override void Apply(float value)
        {
            target.pivot = Vector2.Lerp(from, to, value);
        }

        override public void SaveAsFrom()
        {
            from = target.pivot;
        }

        override public void SaveAsTo()
        {
            to = target.pivot;
        }
    }
}