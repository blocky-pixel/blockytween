﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlockyLib.Tween
{
    [RequireComponent(typeof(CanvasGroup))]
    public class TweenUIGroupAlpha : TweenBehaviour<TweenUIGroupAlphaModule, CanvasGroup>
    {
        [ContextMenu("Read From")] private void ReadFrom() { SaveAsFrom(); }
        [ContextMenu("Read To")] private void ReadTo() { SaveAsTo(); }
    }

    [System.Serializable]
    public class TweenUIGroupAlphaModule : BaseTweenModule<CanvasGroup>
    {
        [Range(0,1)]
        public float from;
        [Range(0,1)]
        public float to;

        public override void Apply(float value)
        {
            target.alpha = Mathf.Lerp(from, to, value);
        }

        override public void SaveAsFrom()
        {
            from = target.alpha;
        }

        override public void SaveAsTo()
        {
            to = target.alpha;
        }
    }
}