﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlockyLib.Tween.UI
{
    [RequireComponent(typeof(Graphic))]
    public class TweenUIColor : TweenBehaviour<TweenUIColorModule, Graphic>
    {
        [ContextMenu("Read From")] private void ReadFrom() { SaveAsFrom(); }
        [ContextMenu("Read To")] private void ReadTo() { SaveAsTo(); }
    }

    [System.Serializable]
    public class TweenUIColorModule : BaseTweenModule<Graphic>
    {
        public Color from = Color.white;
        public Color to = Color.white;

        public override void Apply(float value)
        {
            target.color = Color.Lerp(from, to, value);
        }

        override public void SaveAsFrom()
        {
            from = target.color;
        }

        override public void SaveAsTo()
        {
            to = target.color;
        }
    }
}