﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BlockyLib.Tween.UI
{
    [RequireComponent(typeof(LayoutElement))]
    public class TweenUILayoutWeight : TweenBehaviour<TweenUILayoutWeightModule, LayoutElement>
    {
        [ContextMenu("Read From")] private void ReadFrom() { SaveAsFrom(); }
        [ContextMenu("Read To")] private void ReadTo() { SaveAsTo(); }
    }

    [System.Serializable]
    public class TweenUILayoutWeightModule : BaseTweenModule<LayoutElement>
    {
        public bool height, width;

        public Vector2 from = Vector2.one;
        public Vector2 to = Vector2.one;

        override public void Apply(float value)
        {
            if (height) target.flexibleHeight = Mathf.Lerp(from.y, to.y, value);
            if (width) target.flexibleWidth = Mathf.Lerp(from.x, to.x, value);
        }

        override public void SaveAsFrom()
        {
            from.y = target.flexibleHeight;
            from.x = target.flexibleWidth;
        }

        override public void SaveAsTo()
        {
            to.y = target.flexibleHeight;
            to.x = target.flexibleWidth;
        }
    }
}