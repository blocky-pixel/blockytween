﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace BlockyLib.Tween
{
    [CustomEditor(typeof(TweenBehaviour), true)]
    [CanEditMultipleObjects]
    public class TweenEditor : Editor
    {
        private float defaultPosition;
        private float previewPosition;
        private bool isDirty;

        void OnEnable()
        {
            isDirty = false;
            defaultPosition = 0;
        }

        void OnDisable()
        {
            if (!isDirty || target == null) return;

            var tween = target as TweenBehaviour;
            tween.SetProgress(defaultPosition);
            isDirty = false;
        }

        override public void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var lastValue = previewPosition;
            previewPosition = EditorGUILayout.Slider("Preview Tween", previewPosition,0,1);
            if (previewPosition != lastValue)
            {
                var tween = target as TweenBehaviour;
                if (tween.isActiveAndEnabled)
                {
                    isDirty = true;
                    tween.SetProgress(previewPosition);
                }
            }
        }
    }
}