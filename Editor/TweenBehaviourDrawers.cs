﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace BlockyLib.Tween
{
    public class TweenBehaviourDrawer : PropertyDrawer
    {
        override public void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            //TODO: Read to/from
            var singleFieldRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);

            if (GUI.Button(singleFieldRect, "Read From"))
                Debug.Log("From");
            singleFieldRect.y += EditorGUIUtility.singleLineHeight;

            if (GUI.Button(singleFieldRect, "Read To"))
                Debug.Log("To");

            singleFieldRect.y += EditorGUIUtility.singleLineHeight;
            EditorGUI.PropertyField(singleFieldRect, property, true);
        }
    }
}
